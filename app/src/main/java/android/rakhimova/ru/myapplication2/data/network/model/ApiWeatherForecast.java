package android.rakhimova.ru.myapplication2.data.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiWeatherForecast {

    @SerializedName("list")
    private List<ApiWeatherDay> items;

    public ApiWeatherForecast(List<ApiWeatherDay> items) {
        this.items = items;
    }

    public List<ApiWeatherDay> getItems() {
        return items;
    }
}