package android.rakhimova.ru.myapplication2.domain.main.interactor;

import android.content.Intent;
import android.rakhimova.ru.myapplication2.data.database.DataSource;
import android.rakhimova.ru.myapplication2.data.network.service.WeatherService;
import android.rakhimova.ru.myapplication2.domain.entity.WeatherDay;
import android.rakhimova.ru.myapplication2.ui.UserPreferences;
import android.rakhimova.ru.myapplication2.ui.main.view.WeatherView;

import static android.rakhimova.ru.myapplication2.constant.Constants.CITY;

public class WeatherInteractorImpl implements WeatherInteractor {

    private DataSource dataSource;
    private String city;

    public WeatherInteractorImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void startLoadWeather(WeatherView weatherView) {
        UserPreferences userPreferences = weatherView.getUserPreferences();
        city = userPreferences.getCity();
        Intent intent = new Intent(weatherView.getActivity(), WeatherService.class);
        intent.putExtra(CITY, city);
        weatherView.getActivity().startService(intent);
    }

    @Override
    public WeatherDay getWeatherDay(WeatherView weatherView) {
        return dataSource.getDataReader().getWeatherDay(weatherView.getActivity(), city);
    }

}
