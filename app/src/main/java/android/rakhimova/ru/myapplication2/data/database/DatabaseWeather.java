package android.rakhimova.ru.myapplication2.data.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import static android.rakhimova.ru.myapplication2.constant.Constants.COLUMN_HUMIDITY;
import static android.rakhimova.ru.myapplication2.constant.Constants.COLUMN_PRESSURE;
import static android.rakhimova.ru.myapplication2.constant.Constants.COLUMN_WIND;
import static android.rakhimova.ru.myapplication2.constant.Constants.TABLE_WEATHER;
import static android.rakhimova.ru.myapplication2.constant.Constants.TAG;

public class DatabaseWeather extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "weather.db";
    private static final int DATABASE_VERSION = 3;

    DatabaseWeather(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE if not exists city (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT);");
        db.execSQL("CREATE TABLE if not exists weather (id INTEGER PRIMARY KEY AUTOINCREMENT, id_city INTEGER, weather_image INTEGER, temperature TEXT, wind TEXT, pressure TEXT, humidity TEXT);");
        Log.i(TAG, "onCreate");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion == 2 && newVersion == 3) {
            String upgradeQuery = "ALTER TABLE " + TABLE_WEATHER + " ADD COLUMN " + COLUMN_WIND + " TEXT";
            db.execSQL(upgradeQuery);
            upgradeQuery = "ALTER TABLE " + TABLE_WEATHER + " ADD COLUMN " + COLUMN_PRESSURE + " TEXT";
            db.execSQL(upgradeQuery);
            upgradeQuery = "ALTER TABLE " + TABLE_WEATHER + " ADD COLUMN " + COLUMN_HUMIDITY + " TEXT";
            db.execSQL(upgradeQuery);
        }
    }

}
