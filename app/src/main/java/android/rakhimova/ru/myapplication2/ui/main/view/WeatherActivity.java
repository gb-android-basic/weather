package android.rakhimova.ru.myapplication2.ui.main.view;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.rakhimova.ru.myapplication2.R;
import android.rakhimova.ru.myapplication2.data.database.DataSource;
import android.rakhimova.ru.myapplication2.domain.main.interactor.WeatherInteractor;
import android.rakhimova.ru.myapplication2.domain.main.interactor.WeatherInteractorImpl;
import android.rakhimova.ru.myapplication2.ui.UserPreferences;
import android.rakhimova.ru.myapplication2.ui.citylist.view.CityListActivity;
import android.rakhimova.ru.myapplication2.ui.main.presenter.WeatherPresenter;
import android.rakhimova.ru.myapplication2.ui.main.presenter.WeatherPresenterImpl;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import static android.rakhimova.ru.myapplication2.constant.Constants.CITY;

public class WeatherActivity extends AppCompatActivity implements WeatherView {

    private WeatherFragment weatherFragment;
    private WeatherPresenter presenter;
    private DataSource dataSource;

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_detail);
        initDataSource();
        createPresenter();
        initUI();
        initFragments();
    }

    private void initDataSource() {
        dataSource = new DataSource(getApplicationContext());
        dataSource.open();
    }

    private void createPresenter() {
        WeatherInteractor interactor = new WeatherInteractorImpl(dataSource);
        presenter = new WeatherPresenterImpl(this, interactor);
    }

    private void initUI() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void initFragments() {
        UserPreferences userPreferences = new UserPreferences(this);
        weatherFragment = (WeatherFragment) getSupportFragmentManager().findFragmentById(R.id.weather_detail_fragment);
        if (weatherFragment == null) return;
        Bundle intentBundle = getIntent().getExtras();
        String city;
        if (intentBundle == null) {
            city = userPreferences.getCity();
            weatherFragment.showCity(city);
        } else {
            city = intentBundle.getString(CITY);
            userPreferences.setCity(city);
        }
        if (city == null) {
            presenter.fillFragmentWithEmpty();
        }
    }

    @Override
    public WeatherFragment getFragment() {
        return weatherFragment;
    }

    public WeatherActivity getActivity() {
        return this;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        presenter.onOptionsItemMenu(item);
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void openSearchCityScreen() {
        Intent intent = new Intent(getApplicationContext(), CityListActivity.class);
        startActivity(intent);
    }

    @Override
    public void onRegisterReceiver(BroadcastReceiver receiver, IntentFilter intentFilter) {
        registerReceiver(receiver, intentFilter);
    }

    @Override
    public void onUnregisterReceiver(BroadcastReceiver receiver) {
        unregisterReceiver(receiver); //FIXME Ошибка при повороте экрана
    }

    @Override
    public UserPreferences getUserPreferences() {
        return new UserPreferences(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onStop();
    }

}