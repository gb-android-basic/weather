package android.rakhimova.ru.myapplication2.data.network.service;

import android.app.IntentService;
import android.content.Intent;
import android.rakhimova.ru.myapplication2.R;
import android.rakhimova.ru.myapplication2.data.database.DataSource;
import android.rakhimova.ru.myapplication2.data.network.model.ApiWeatherDay;
import android.rakhimova.ru.myapplication2.data.network.model.ApiWeatherForecast;
import android.support.annotation.Nullable;
import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.rakhimova.ru.myapplication2.constant.Constants.ACTION_UPDATE_WEATHER;
import static android.rakhimova.ru.myapplication2.constant.Constants.CITY;
import static android.rakhimova.ru.myapplication2.constant.Constants.KEY;
import static android.support.constraint.Constraints.TAG;

public class WeatherService extends IntentService {

    private static final String UNITS = "metric";
    private static final String BACKGROUND_THREAD = "backgroundThread";
    private WeatherDataLoader.ApiInterface api;
    private String city;
    private int weatherImage;
    private String temperature;
    private String pressure;
    private String humidity;
    private String wind;
    private DataSource dataSource;

    public WeatherService() {
        super(BACKGROUND_THREAD);
    }

    public void getWeather() {
        initDataSource();
        getWeatherToday();
    }

    private void getWeatherToday() {
        Call<ApiWeatherDay> callToday = api.getToday(city, UNITS, KEY);
        callToday.enqueue(new Callback<ApiWeatherDay>() {
            @Override
            public void onResponse(Call<ApiWeatherDay> call, Response<ApiWeatherDay> response) {
                ApiWeatherDay data = response.body();
                if (response.isSuccessful()) {
                    setWeatherIcon(data.getIcon());
                    temperature = data.getTemp();
                    pressure = data.getPressure();
                    humidity = data.getHumidity();
                    wind = data.getWind();
                    addWeatherToDatabase();
                    sendBroadcast();
                }
            }
            @Override
            public void onFailure(Call<ApiWeatherDay> call, Throwable t) {
                Log.e(TAG, "onFailure");
                Log.e(TAG, t.toString());
            }
        });
    }

    private void getWeatherForecast() {
        Call<ApiWeatherForecast> callForecast = api.getForecast(city, UNITS, KEY);
        callForecast.enqueue(new Callback<ApiWeatherForecast>() {
            @Override
            public void onResponse(Call<ApiWeatherForecast> call, Response<ApiWeatherForecast> response) {
                ApiWeatherForecast data = response.body();
                if (response.isSuccessful()) {
                    //TODO Реализовать загрузку прогноза на несколько дней
                }
            }

            @Override
            public void onFailure(Call<ApiWeatherForecast> call, Throwable t) {
                Log.e(TAG, "onFailure");
                Log.e(TAG, t.toString());
            }
        });
    }

    private void initDataSource() {
        dataSource = new DataSource(getApplicationContext());
        dataSource.open();
    }

    public void setWeatherIcon(String icon) {
        String idIcon = icon.substring(0, 2);
        weatherImage = R.drawable.question;
        if (idIcon.equals("01")) weatherImage = R.drawable.img_sun;
        if (idIcon.equals("02")) weatherImage = R.drawable.few_cloudy;
        if (idIcon.equals("03")) weatherImage = R.drawable.img_cloud;
        if (idIcon.equals("04")) weatherImage = R.drawable.img_cloud;
        if (idIcon.equals("09")) weatherImage = R.drawable.shower_rain;
        if (idIcon.equals("10")) weatherImage = R.drawable.img_rain;
        if (idIcon.equals("11")) weatherImage = R.drawable.thunderstorm;
        if (idIcon.equals("13")) weatherImage = R.drawable.snow;
        if (idIcon.equals("50")) weatherImage = R.drawable.mist;
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        api = WeatherDataLoader.getClient().create(WeatherDataLoader.ApiInterface.class);
        if (intent != null) city = intent.getStringExtra(CITY);
        getWeather();
    }

    private void addWeatherToDatabase() {
        dataSource.addWeather(city, weatherImage, temperature, wind, pressure, humidity);
    }

    private void sendBroadcast() {
        Intent responseIntent = new Intent();
        responseIntent.setAction(ACTION_UPDATE_WEATHER);
        responseIntent.addCategory(Intent.CATEGORY_DEFAULT);
        sendBroadcast(responseIntent);
    }

}