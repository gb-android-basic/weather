package android.rakhimova.ru.myapplication2.ui.citylist.presenter;

public interface CityListPresenter {

    void onStart();

    void onStop();

    void addCity();

    void refreshRecycler();

    void onListFragmentInteraction(String item);

}
