package android.rakhimova.ru.myapplication2.domain.citylist.interactor;

public interface CityListInteractor {

    void addCity(String city);

}
