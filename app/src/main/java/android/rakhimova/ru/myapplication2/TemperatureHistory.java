package android.rakhimova.ru.myapplication2;

import android.os.Bundle;
import android.rakhimova.ru.myapplication2.ui.adapter.HistoryAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static android.rakhimova.ru.myapplication2.constant.Constants.CITY;
import static android.rakhimova.ru.myapplication2.constant.Constants.DEGREES;

public class TemperatureHistory extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature_history);
        initUi();
    }

    private void initUi() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            CharSequence degrees = bundle.getCharSequence(DEGREES);
            //String img_weather = (String) bundle.getCharSequence(IMAGE_WEATHER); //FIXME Извлечь полученный путь к картинке
            CharSequence city = bundle.getCharSequence(CITY);
            TextView textDegrees = findViewById(R.id.textViewDegrees);
            TextView textCity = findViewById(R.id.textViewCity);
            textCity.setText(city);
            textDegrees.setText(degrees);
        }
        ImageView imageWeather = findViewById(R.id.imageWeatherHistory);
        imageWeather.setImageResource(R.drawable.img_sun);
        initOurList();
    }

    private void initOurList() {
        String[] dayList = getResources().getStringArray(R.array.day_list);
        List<String> stringList = new LinkedList<>();
        stringList.addAll(Arrays.asList(dayList).subList(0, 7));

        RecyclerView recyclerView = findViewById(R.id.list_history_container);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        HistoryAdapter historyAdapter = new HistoryAdapter(stringList);
        recyclerView.setAdapter(historyAdapter);
    }

}
