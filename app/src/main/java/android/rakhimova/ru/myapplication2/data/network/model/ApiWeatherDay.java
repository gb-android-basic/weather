package android.rakhimova.ru.myapplication2.data.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.Calendar;
import java.util.List;

public class ApiWeatherDay {

    @SerializedName("main")
    private WeatherMain main;

    @SerializedName("weather")
    private List<WeatherDescription> description;

    @SerializedName("name")
    private String city;

    @SerializedName("dt")
    private long timestamp;

    @SerializedName("wind")
    private WeatherWind wind;

    public ApiWeatherDay(WeatherMain main, List<WeatherDescription> description, WeatherWind wind) {
        this.main = main;
        this.description = description;
        this.wind = wind;
    }

    public Calendar getDate() {
        Calendar date = Calendar.getInstance();
        date.setTimeInMillis(timestamp * 1000);
        return date;
    }

    public String getTemp() {
        return String.valueOf(main.temp);
    }

    public String getPressure() {
        return String.valueOf(main.pressure);
    }

    public String getHumidity() {
        return String.valueOf(main.humidity);
    }

    public String getWind() {
        return String.valueOf(wind.speed);
    }

    public String getCity() {
        return city;
    }

    public String getIcon() {
        return description.get(0).icon;
    }

    public class WeatherMain {
        Double temp;
        Double humidity;
        Double pressure;
    }

    public class WeatherDescription {
        String icon;

    }

    private class WeatherWind {
        Double speed;
    }
}
