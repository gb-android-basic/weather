package android.rakhimova.ru.myapplication2.ui.citylist.view;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;

public interface CityListView {

    void openWeatherScreen(String city);

    void onRegisterReceiver(BroadcastReceiver receiver, IntentFilter intentFilter);

    void onUnregisterReceiver(BroadcastReceiver receiver);

    void onRefreshRecycler();

    CityListActivity getActivity();

}
