package android.rakhimova.ru.myapplication2.domain.entity;

import android.support.annotation.NonNull;

public class OneCity {
    public final long id;
    public final String content;

    public OneCity(long id, String content) {
        this.id = id;
        this.content = content;
    }

    @NonNull
    @Override
    public String toString() {
        return "OneCity{" +
                "id='" + id + '\'' +
                ", content='" + content + '}';
    }
}