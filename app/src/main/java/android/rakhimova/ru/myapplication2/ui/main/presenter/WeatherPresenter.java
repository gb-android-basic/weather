package android.rakhimova.ru.myapplication2.ui.main.presenter;

import android.view.MenuItem;

public interface WeatherPresenter {

    void onStart();

    void onStop();

    void startLoadWeather();

    void fillFragmentWithEmpty();

    void getWeatherDay();

    void searchCity();

    void onOptionsItemMenu(MenuItem item);
}
