package android.rakhimova.ru.myapplication2.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.rakhimova.ru.myapplication2.ui.citylist.presenter.CityListPresenter;
import android.rakhimova.ru.myapplication2.ui.main.presenter.WeatherPresenter;

public class WeatherBroadcastReceiver<T> extends BroadcastReceiver {

    private T presenter;

    public WeatherBroadcastReceiver(T presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (presenter instanceof WeatherPresenter) {
            ((WeatherPresenter) presenter).getWeatherDay();
        } else if (presenter instanceof CityListPresenter) {
            ((CityListPresenter) presenter).refreshRecycler();
        }
    }
}
