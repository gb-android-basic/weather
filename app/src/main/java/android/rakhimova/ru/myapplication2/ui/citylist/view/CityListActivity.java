package android.rakhimova.ru.myapplication2.ui.citylist.view;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.rakhimova.ru.myapplication2.R;
import android.rakhimova.ru.myapplication2.data.database.DataSource;
import android.rakhimova.ru.myapplication2.domain.citylist.interactor.CityListInteractor;
import android.rakhimova.ru.myapplication2.domain.citylist.interactor.CityListInteractorImpl;
import android.rakhimova.ru.myapplication2.ui.citylist.presenter.CityListPresenter;
import android.rakhimova.ru.myapplication2.ui.citylist.presenter.CityListPresenterImpl;
import android.rakhimova.ru.myapplication2.ui.main.view.WeatherActivity;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;

import static android.rakhimova.ru.myapplication2.constant.Constants.CITY;

public class CityListActivity extends AppCompatActivity implements CityListFragment.OnListFragmentInteractionListener, CityListView {

    private DataSource dataSource;
    private CityListFragment cityName;
    private CityListPresenter presenter;

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_name);
        initFragments();
        initDataSource();
        createPresenter();
        FloatingActionButton addCity = findViewById(R.id.fab);
        addCity.setOnClickListener(view -> presenter.addCity());
    }

    private void createPresenter() {
        CityListInteractor interactor = new CityListInteractorImpl(dataSource);
        presenter = new CityListPresenterImpl(this, interactor);
    }

    private void initDataSource() {
        dataSource = new DataSource(getApplicationContext());
        dataSource.open();
    }

    private void initFragments() {
        cityName = (CityListFragment) getSupportFragmentManager().findFragmentById(R.id.city_name_fragment);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle saveInstanceState) {
        super.onSaveInstanceState(saveInstanceState);
    }

    @Override
    public void onListFragmentInteraction(String item) {
        presenter.onListFragmentInteraction(item);
    }

    @Override
    public void openWeatherScreen(String city) {
        Intent intent = new Intent(getApplicationContext(), WeatherActivity.class);
        intent.putExtra(CITY, city);
        startActivity(intent);
    }

    @Override
    public void onRegisterReceiver(BroadcastReceiver receiver, IntentFilter intentFilter) {
        registerReceiver(receiver, intentFilter);
    }

    @Override
    public void onUnregisterReceiver(BroadcastReceiver receiver) {
        unregisterReceiver(receiver); //FIXME Ошибка при повороте экрана
    }

    @Override
    public void onRefreshRecycler() {
        cityName.refreshRecycler();
    }

    @Override
    public CityListActivity getActivity() {
        return this;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onStop();
    }
}
