package android.rakhimova.ru.myapplication2.data.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.rakhimova.ru.myapplication2.constant.Constants;

import java.io.Closeable;

import static android.rakhimova.ru.myapplication2.constant.Constants.ACTION_UPDATE_WEATHER;

public class DataSource implements Closeable {

    private DatabaseWeather dbHelper;
    private static SQLiteDatabase database;
    private DataReader dataReader;
    private Context context;

    public DataSource(Context context) {
        dbHelper = new DatabaseWeather(context);
        this.context = context;
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
        dataReader = new DataReader(database);
        dataReader.open();
    }

    public void close() {
        dataReader.close();
        dbHelper.close();
    }

    static int getIdCity(String city) {
        String selection = "name = ?";
        String[] selectionArgs = {city};
        Cursor cursor = database.query(Constants.TABLE_CITY, Constants.citiesAllColumn, selection, selectionArgs, null, null, null);
        cursor.moveToFirst();
        int idCity = 0;
        if (cursor.isFirst()) idCity = cursor.getInt(0);
        cursor.close();
        return idCity;
    }

    public void addCity(String name) {
        ContentValues values = new ContentValues();
        values.put(Constants.COLUMN_CITY, name);
        long result = database.insert(Constants.TABLE_CITY, null, values); //TODO Сделать проверку повторного добавления города в БД
        if (result > 0) sendBroadcastNewCity();
    }

    private void sendBroadcastNewCity() {
        Intent responseIntent = new Intent();
        responseIntent.setAction(ACTION_UPDATE_WEATHER);
        responseIntent.addCategory(Intent.CATEGORY_DEFAULT);
        context.sendBroadcast(responseIntent);
    }

    public void addWeather(String city, int idIcon, String temperature, String wind, String pressure, String humidity) {
        int idCity = getIdCity(city);
        String selection = Constants.COLUMN_ID_CITY + " = ?";
        String[] argIdCity = {String.valueOf(idCity)};
        ContentValues values = new ContentValues();
        values.put(Constants.COLUMN_ID_CITY, idCity);
        values.put(Constants.COLUMN_WEATHER_IMAGE, idIcon);
        values.put(Constants.COLUMN_TEMPERATURE, temperature);
        values.put(Constants.COLUMN_WIND, wind);
        values.put(Constants.COLUMN_PRESSURE, pressure);
        values.put(Constants.COLUMN_HUMIDITY, humidity);
        Cursor cursor = dataReader.checkWeatherDay(context, city);
        if (cursor.getCount() == 0) database.insert(Constants.TABLE_WEATHER, null, values);
        else database.update(Constants.TABLE_WEATHER, values, selection, argIdCity);
    }

    public DataReader getDataReader() {
        return dataReader;
    }
}