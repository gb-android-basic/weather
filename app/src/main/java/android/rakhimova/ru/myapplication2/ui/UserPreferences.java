package android.rakhimova.ru.myapplication2.ui;

import android.app.Activity;
import android.content.SharedPreferences;
import android.rakhimova.ru.myapplication2.R;

import static android.rakhimova.ru.myapplication2.constant.Constants.CITY;

public class UserPreferences {

    private static final String CITY_DEFAULT = String.valueOf(R.string.city);
    private SharedPreferences sharedPreferences;

    public UserPreferences(Activity activity) {
        sharedPreferences = activity.getPreferences(Activity.MODE_PRIVATE);
    }

    public String getCity() {
        return sharedPreferences.getString(CITY, CITY_DEFAULT);
    }

    public void setCity(String city) {
        sharedPreferences.edit().putString(CITY, city).apply();
    }

}
