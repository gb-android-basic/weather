package android.rakhimova.ru.myapplication2.ui.main.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.rakhimova.ru.myapplication2.R;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class WeatherFragment extends Fragment {

    private ImageView image;
    private TextView valuePrecipitation;
    private TextView valuePressure;
    private TextView valueWind;
    private TextView cityTextView;
    private TextView degreesTextView;

    public WeatherFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_weather_detail, container, false);
        image = view.findViewById(R.id.imageViewWeather);
        valuePrecipitation = view.findViewById(R.id.textViewHumidity);
        valuePressure = view.findViewById(R.id.textViewPressure);
        valueWind = view.findViewById(R.id.textViewWind);
        cityTextView = view.findViewById(R.id.textViewCity);
        degreesTextView = view.findViewById(R.id.textViewDegrees);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void changeImage(int id) {
        Glide.with(this)
                .load(id)
                .into(image);
    }

    @SuppressLint("SetTextI18n")
    public void showOptionHumidity(String humidity) {
        valuePrecipitation.setText(humidity + getString(R.string.humidity));
    }

    @SuppressLint("SetTextI18n")
    public void showOptionPressure(String pressure) {
        valuePressure.setText(pressure + getString(R.string.pressure));
    }

    @SuppressLint("SetTextI18n")
    public void showOptionWind(String wind) {
        valueWind.setText(wind + getString(R.string.wind));
    }

    public void showCity(String city) {
        cityTextView.setText(city);
    }

    @SuppressLint("SetTextI18n")
    public void showTemperature(String temperature) {
        degreesTextView.setText(temperature + getString(R.string.sign_degree));
    }

}

