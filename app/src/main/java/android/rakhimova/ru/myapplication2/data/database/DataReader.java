package android.rakhimova.ru.myapplication2.data.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.rakhimova.ru.myapplication2.constant.Constants;
import android.rakhimova.ru.myapplication2.domain.entity.WeatherDay;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.List;

import static android.rakhimova.ru.myapplication2.constant.Constants.COLUMN_CITY;
import static android.rakhimova.ru.myapplication2.constant.Constants.COLUMN_HUMIDITY;
import static android.rakhimova.ru.myapplication2.constant.Constants.COLUMN_ID_CITY;
import static android.rakhimova.ru.myapplication2.constant.Constants.COLUMN_PRESSURE;
import static android.rakhimova.ru.myapplication2.constant.Constants.COLUMN_TEMPERATURE;
import static android.rakhimova.ru.myapplication2.constant.Constants.COLUMN_WEATHER_IMAGE;
import static android.rakhimova.ru.myapplication2.constant.Constants.COLUMN_WIND;
import static android.rakhimova.ru.myapplication2.constant.Constants.TABLE_CITY;
import static android.rakhimova.ru.myapplication2.constant.Constants.TABLE_WEATHER;
import static android.rakhimova.ru.myapplication2.constant.Constants.citiesAllColumn;
import static android.rakhimova.ru.myapplication2.constant.Constants.weatherAllColumn;

public class DataReader implements Closeable {

    private Cursor cursor;
    private SQLiteDatabase database;
    private String currentHumidity;
    private String currentPressure;
    private String currentTemp;
    private String currentWind;
    private int currentIdCity;
    private int currentImage;
    private WeatherDay weatherDay;

    DataReader(SQLiteDatabase database) {
        this.database = database;
    }

    void open() {
        query();
        cursor.moveToFirst();
    }

    private void query() {
        cursor = database.query(TABLE_CITY,
                citiesAllColumn, null, null, null, null, null);
    }

    public void close() {
        cursor.close();
    }

    public List<String> getCityList(Context context) {
        List<String> cityList = new ArrayList<>();
        DatabaseWeather databaseWeather = new DatabaseWeather(context);
        database = databaseWeather.getReadableDatabase();
        cursor = database.query(TABLE_CITY,
                citiesAllColumn,
                null,
                null,
                COLUMN_CITY,
                null,
                null);
        int nameColumnIndex = cursor.getColumnIndex(COLUMN_CITY);
        while (cursor.moveToNext()) {
            String currentName = cursor.getString(nameColumnIndex);
            cityList.add(currentName);
        }
        close();
        return cityList;
    }

    public WeatherDay getWeatherDay(Context context, String city) {
        Cursor cursor = checkWeatherDay(context, city);
        if (cursor.getCount() == 0) return weatherDay;
        int idCityColumnIndex = cursor.getColumnIndex(COLUMN_ID_CITY);
        int imageColumnIndex = cursor.getColumnIndex(COLUMN_WEATHER_IMAGE);
        int temperatureColumnIndex = cursor.getColumnIndex(COLUMN_TEMPERATURE);
        int windColumnIndex = cursor.getColumnIndex(COLUMN_WIND);
        int pressureColumnIndex = cursor.getColumnIndex(COLUMN_PRESSURE);
        int humidityColumnIndex = cursor.getColumnIndex(COLUMN_HUMIDITY);
        cursor.moveToLast();
        currentIdCity = cursor.getInt(idCityColumnIndex);
        currentImage = cursor.getInt(imageColumnIndex);
        currentTemp = cursor.getString(temperatureColumnIndex);
        currentWind = cursor.getString(windColumnIndex);
        currentPressure = cursor.getString(pressureColumnIndex);
        currentHumidity = cursor.getString(humidityColumnIndex);
        initWeatherDay(city);
        cursor.close();
        return weatherDay;
    }

    Cursor checkWeatherDay(Context context, String city) {
        DatabaseWeather databaseWeather = new DatabaseWeather(context);
        database = databaseWeather.getReadableDatabase();
        String selection = Constants.COLUMN_ID_CITY + " = ? ";
        int idCity = DataSource.getIdCity(city);
        String[] argIdCity = {String.valueOf(idCity)};
        cursor = database.query(TABLE_WEATHER,
                weatherAllColumn,
                selection,
                argIdCity,
                null,
                null,
                null);
        return cursor;
    }

    private void initWeatherDay(String city) {
        weatherDay = new WeatherDay();
        weatherDay.setIdCity(currentIdCity);
        weatherDay.setCity(city);
        weatherDay.setImage(currentImage);
        weatherDay.setTemp(currentTemp);
        weatherDay.setWind(currentWind);
        weatherDay.setPressure(currentPressure);
        weatherDay.setHumidity(currentHumidity);
    }

}


