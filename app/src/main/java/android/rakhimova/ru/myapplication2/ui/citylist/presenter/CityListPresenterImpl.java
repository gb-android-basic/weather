package android.rakhimova.ru.myapplication2.ui.citylist.presenter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentFilter;
import android.rakhimova.ru.myapplication2.R;
import android.rakhimova.ru.myapplication2.domain.citylist.interactor.CityListInteractor;
import android.rakhimova.ru.myapplication2.ui.WeatherBroadcastReceiver;
import android.rakhimova.ru.myapplication2.ui.citylist.view.CityListView;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import static android.rakhimova.ru.myapplication2.constant.Constants.ACTION_UPDATE_WEATHER;

public class CityListPresenterImpl implements CityListPresenter {

    private CityListView cityListView;
    private CityListInteractor cityListInteractor;
    private WeatherBroadcastReceiver cityListBroadcastReceiver;

    public CityListPresenterImpl(CityListView cityListView, CityListInteractor cityListInteractor) {
        this.cityListInteractor = cityListInteractor;
        this.cityListView = cityListView;
    }

    @Override
    public void onStart() {
        initReceiver();
    }

    @Override
    public void onStop() {
        cityListView.onUnregisterReceiver(cityListBroadcastReceiver);
    }

    @Override
    public void addCity() {
        LayoutInflater factory = LayoutInflater.from(cityListView.getActivity());
        @SuppressLint("InflateParams") final View alertView = factory.inflate(R.layout.layout_add_city, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(cityListView.getActivity());
        builder.setView(alertView);
        builder.setTitle(R.string.alert_city_add);
        builder.setNegativeButton(R.string.alert_cancel, null);
        builder.setPositiveButton(R.string.menu_add, (dialog, id) -> {  //TODO Сделать проверку на существование города в БД OpenWeatherMap
            EditText newCityName = alertView.findViewById(R.id.editTextCity);
            String city = newCityName.getText().toString();
            cityListInteractor.addCity(city);
        });
        builder.show();
    }

    private void initReceiver() {
        cityListBroadcastReceiver = new WeatherBroadcastReceiver<>(this);
        IntentFilter intentFilter = new IntentFilter(ACTION_UPDATE_WEATHER);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        cityListView.onRegisterReceiver(cityListBroadcastReceiver, intentFilter);
    }

    @Override
    public void refreshRecycler() {
        cityListView.onRefreshRecycler();
    }

    @Override
    public void onListFragmentInteraction(String item) {
        cityListView.openWeatherScreen(item);
    }

}
