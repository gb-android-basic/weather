package android.rakhimova.ru.myapplication2.domain.citylist.interactor;

import android.rakhimova.ru.myapplication2.data.database.DataSource;

public class CityListInteractorImpl implements CityListInteractor {

    private DataSource dataSource;

    public CityListInteractorImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void addCity(String city) {
        dataSource.addCity(city);
    }
}
