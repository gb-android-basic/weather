package android.rakhimova.ru.myapplication2.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.rakhimova.ru.myapplication2.R;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {

    private final List<String> stringList;

    public HistoryAdapter(List<String> list) {
        this.stringList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.history_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Context context = viewHolder.textViewDegreesHistory.getContext();
        viewHolder.textViewDay.setText(stringList.get(i));
        viewHolder.textViewDegreesHistory.setText(i + context.getString(R.string.sign_degree));
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView textViewDegreesHistory;
        TextView textViewDay;
        View itemView;

        private ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.textViewDay = itemView.findViewById(R.id.textViewDay);
            this.textViewDegreesHistory = itemView.findViewById(R.id.textViewDegreesHistory);
            this.itemView = itemView;
        }
    }
}
