package android.rakhimova.ru.myapplication2.domain.main.interactor;

import android.rakhimova.ru.myapplication2.domain.entity.WeatherDay;
import android.rakhimova.ru.myapplication2.ui.main.view.WeatherView;

public interface WeatherInteractor {

    void startLoadWeather(WeatherView weatherView);

    WeatherDay getWeatherDay(WeatherView weatherView);

}
