package android.rakhimova.ru.myapplication2.data.network.service;

import android.rakhimova.ru.myapplication2.data.network.model.ApiWeatherDay;
import android.rakhimova.ru.myapplication2.data.network.model.ApiWeatherForecast;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

class WeatherDataLoader {

    private static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";
    private static Retrofit retrofit = null;

    static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public interface ApiInterface {
        @GET("weather")
        Call<ApiWeatherDay> getToday(
                @Query("q") String city,
                @Query("units") String units,
                @Query("appid") String appid
        );

        @GET("forecast")
        Call<ApiWeatherForecast> getForecast(
                @Query("q") String city,
                @Query("units") String units,
                @Query("appid") String appid
        );
    }
}