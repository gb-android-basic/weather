package android.rakhimova.ru.myapplication2.constant;

public class Constants {

    public static final String CITY = "city";
    public static final String DEGREES = "degrees";
    public static final String WIND = "wind";
    public static final String PRESSURE = "pressure";
    public static final String HUMIDITY = "humidity";
    public static final String WEATHER_IMAGE = "weatherImage";
    public static final String KEY = "241c1d5ab030aa481966b6c32063c077";
    public static final String TAG = "WEATHER7";

    public static final String COLUMN_WIND = "wind";
    public static final String TABLE_CITY = "city";
    public static final String COLUMN_CITY = "name";
    public static final String TABLE_WEATHER = "weather";
    public static final String COLUMN_ID_CITY = "id_city";
    public static final String COLUMN_WEATHER_IMAGE = "weather_image";
    public static final String COLUMN_TEMPERATURE = "temperature";
    public static final String COLUMN_PRESSURE = "pressure";
    public static final String COLUMN_HUMIDITY = "humidity";
    private static final String COLUMN_ID = "id";

    public static final String ACTION_UPDATE_WEATHER = "WEATHER_UPDATED";

    public static String[] citiesAllColumn = {
            COLUMN_ID,
            COLUMN_CITY
    };
    public static String[] weatherAllColumn = {
            COLUMN_ID,
            COLUMN_ID_CITY,
            COLUMN_WEATHER_IMAGE,
            COLUMN_TEMPERATURE,
            COLUMN_WIND,
            COLUMN_PRESSURE,
            COLUMN_HUMIDITY
    };


}
