package android.rakhimova.ru.myapplication2.ui.citylist.view;

import android.content.Context;
import android.os.Bundle;
import android.rakhimova.ru.myapplication2.R;
import android.rakhimova.ru.myapplication2.data.database.DataSource;
import android.rakhimova.ru.myapplication2.ui.adapter.CityListAdapter;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class CityListFragment extends Fragment {

    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private DataSource dataSource;
    private RecyclerView recyclerView;
    private Context context;

    public CityListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        initDataSource();
    }

    private void initDataSource() {
        dataSource = new DataSource(getContext());
        dataSource.open();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cityname_list, container, false);
        context = view.getContext();
        recyclerView = view.findViewById(R.id.list);
        showCityRecycler();
        return view;
    }

    private void showCityRecycler() {
        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }
        recyclerView.setAdapter(new CityListAdapter(dataSource.getDataReader().getCityList(context), mListener));
    }

    public void refreshRecycler() {
        recyclerView.setAdapter(new CityListAdapter(dataSource.getDataReader().getCityList(context), mListener));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(final String item);
    }
}
