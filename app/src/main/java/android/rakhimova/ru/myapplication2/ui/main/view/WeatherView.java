package android.rakhimova.ru.myapplication2.ui.main.view;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.rakhimova.ru.myapplication2.ui.UserPreferences;

public interface WeatherView {

    WeatherActivity getActivity();

    WeatherFragment getFragment();

    void openSearchCityScreen();

    void onRegisterReceiver(BroadcastReceiver receiver, IntentFilter intentFilter);

    void onUnregisterReceiver(BroadcastReceiver receiver);

    UserPreferences getUserPreferences();
}
