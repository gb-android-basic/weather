package android.rakhimova.ru.myapplication2.ui.main.presenter;

import android.content.Intent;
import android.content.IntentFilter;
import android.rakhimova.ru.myapplication2.R;
import android.rakhimova.ru.myapplication2.domain.entity.WeatherDay;
import android.rakhimova.ru.myapplication2.domain.main.interactor.WeatherInteractor;
import android.rakhimova.ru.myapplication2.ui.WeatherBroadcastReceiver;
import android.rakhimova.ru.myapplication2.ui.main.view.WeatherView;
import android.view.MenuItem;

import static android.rakhimova.ru.myapplication2.constant.Constants.ACTION_UPDATE_WEATHER;

public class WeatherPresenterImpl implements WeatherPresenter {

    private static final String DEFAULT_NULL = "--";
    private WeatherView weatherView;
    private WeatherInteractor weatherInteractor;
    private WeatherBroadcastReceiver weatherBroadcastReceiver;

    public WeatherPresenterImpl(WeatherView weatherView, WeatherInteractor weatherInteractor) {
        this.weatherInteractor = weatherInteractor;
        this.weatherView = weatherView;
    }

    @Override
    public void onStart() {
        initReceiver();
        startLoadWeather();
    }

    @Override
    public void onStop() {
        weatherView.onUnregisterReceiver(weatherBroadcastReceiver);
    }

    private void initReceiver() {
        weatherBroadcastReceiver = new WeatherBroadcastReceiver<>(this);
        IntentFilter intentFilter = new IntentFilter(
                ACTION_UPDATE_WEATHER);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        weatherView.onRegisterReceiver(weatherBroadcastReceiver, intentFilter);
    }

    @Override
    public void startLoadWeather() {
        weatherInteractor.startLoadWeather(weatherView);
    }

    @Override
    public void getWeatherDay() {
        WeatherDay weatherDay = weatherInteractor.getWeatherDay(weatherView);
        if (weatherDay == null) fillFragmentWithEmpty();
        else fillFragment(weatherDay);
    }

    @Override
    public void searchCity() {
        weatherView.openSearchCityScreen();
    }

    @Override
    public void onOptionsItemMenu(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.search_city) {
            searchCity();
        } else if (id == R.id.refresh) {
            startLoadWeather();
            getWeatherDay();
        }
    }

    @Override
    public void fillFragmentWithEmpty() {
        weatherView.getFragment().showCity(DEFAULT_NULL);
        weatherView.getFragment().showTemperature(DEFAULT_NULL);
        weatherView.getFragment().showOptionWind(DEFAULT_NULL);
        weatherView.getFragment().showOptionPressure(DEFAULT_NULL);
        weatherView.getFragment().showOptionHumidity(DEFAULT_NULL);
        weatherView.getFragment().changeImage(R.drawable.question);
    }

    private void fillFragment(WeatherDay weatherDay) {
        weatherView.getFragment().showCity(weatherDay.getCity());
        weatherView.getFragment().showTemperature(weatherDay.getTemp());
        weatherView.getFragment().showOptionWind(weatherDay.getWind());
        weatherView.getFragment().showOptionPressure(weatherDay.getPressure());
        weatherView.getFragment().showOptionHumidity(weatherDay.getHumidity());
        weatherView.getFragment().changeImage(weatherDay.getImage());
    }

}
